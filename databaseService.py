#!/usr/bin/env python3

import os, sys
from datetime import date
from sqlalchemy import create_engine, distinct, func
from sqlalchemy.orm import sessionmaker
 
from sqlalchemy_declarative import Base, Popis, Potraviny

class DatabaseService():

	def __init__(self):
		if os.path.isfile('database.sqlite'):
			engine = create_engine('sqlite:///database.sqlite')
			Base.metadata.bind = engine
			DBSession = sessionmaker(bind=engine)
			self.session = DBSession()
		else:
			print('ERROR with db file!!!')
			sys.exit(1)


	def get_all_potraviny_id(self):
		return self.session.query(Potraviny.id).all()


	def insert_data(self, data = []):
		potraviny = Potraviny(
			id = data[0],
			name = data[1],
			name_en = data[2],
			name_lat = data[3],
			enerc_kj = data[4],
			enerc_kcal = data[5],
			prot = data[6],
			nt = data[7],
			fat = data[8],
			chot = data[9],
			cho = data[10],
			fibt = data[11],
			water = data[12],
			alc = data[13],
			ash = data[14],
			thia = data[15],
			ribf = data[16],
			niaeq = data[17],
			nia = data[18],
			fol = data[19],
			pantac = data[20],
			pyrxn = data[21],
			vitb12 = data[22],
			vitc = data[23],
			vita = data[24],
			retol = data[25],
			cartb = data[26],
			vitd = data[27],
			vite = data[28],
			tocpha = data[29],
			na = data[30],
			mg = data[31],
			p = data[32],
			k = data[33],
			ca = data[34],
			fe = data[35],
			cu = data[36],
			zn = data[37],
			se = data[38],
			i = data[39],
			nacl = data[40],
			sugar = data[41],
			frus = data[42],
			glus = data[43],
			lacs = data[44],
			mals = data[45],
			sucs = data[46],
			starch = data[47],
			oa = data[48],
			poly = data[49],
			fasat = data[50],
			f4_0 = data[51],
			f6_0 = data[52],
			f8_0 = data[53],
			f10_0 = data[54],
			f12_0 = data[55],
			f13_0 = data[56],
			f14_0 = data[57],
			f15_0 = data[58],
			f16_0 = data[59],
			f17_0 = data[60],
			f18_0 = data[61],
			f20_0 = data[62],
			f22_0 = data[63],
			fams = data[64],
			f14_1cn5 = data[65],
			f16_1cn7 = data[66],
			f17_1 = data[67],
			f18_1cn9 = data[68],
			f20_1cn11 = data[69],
			f22_1cn9 = data[70],
			fapu = data[71],
			facn6 = data[72],
			f18_2cn6 = data[73],
			f18_3cn6 = data[74],
			f20_4cn6 = data[75],
			facn3 = data[76],
			f18_3cn3 = data[77],
			f20_5cn3 = data[78],
			f22_5cn3 = data[79],
			f22_6cn3 = data[80],
			fatrn = data[81],
			f18_1tn9 = data[82],
			f18_2ttn6 = data[83],
			chorl = data[84],
			gly = data[85],
			ala = data[86],
			val = data[87],
			leu = data[88],
			ile = data[89],
			ser = data[90],
			thr = data[91],
			lys = data[92],
			asp = data[93],
			glu = data[94],
			arg = data[95],
			cys = data[96],
			met = data[97],
			phe = data[98],
			tyr = data[99],
			trp = data[100],
			his = data[101],
			pro = data[102])
		self.session.add(potraviny)	
		self.session.commit()
