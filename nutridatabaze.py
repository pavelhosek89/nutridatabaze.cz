#!/usr/bin/env python3

import sys
import time
import urllib.request
from bs4 import BeautifulSoup
from random import randrange
from databaseService import DatabaseService

class Parser():

    def __init__(self):
        self.db = DatabaseService()
        
        self.nums = [n for n in range(800)]
        potraviny = self.db.get_all_potraviny_id()
        for potravina in potraviny:
            if potravina[0] in self.nums:
                self.nums.remove(potravina[0])
        self.num = -1
        print(self.nums)

        while(len(self.nums)!=0):
            self.num = randrange(0,len(self.nums))
            self.parse_page(self.nums[self.num])
            seconds = randrange(15,60)
            time.sleep(seconds)

    def parse_page(self,id):
        try :
            data = []
            url = "http://www.nutridatabaze.cz/potraviny/?id="+str(id)
            web_page = urllib.request.urlopen(url).read()
            soup = BeautifulSoup(web_page, "html.parser")
            content = soup.find('div', attrs={'class':'czfcdb-detail'})
            h1 = content.find('h1')
            h2 = content.find('h2')
            p = content.find('p', attrs={'class':'czfcdb-detail-latin'})
            tab2 = soup.find('div', attrs={'id':'tab-2'})
            if tab2 != None:
                print(url)
                #print(id)
                table = tab2.find('table')

                data.append(int(id))
                data.append(h1.string)
                data.append(h2.string)
                if p != None:
                    data.append(p.string)
                else:
                    data.append('')

                rows = table.findAll('tr')
                for row in rows:
                    cols = row.find_all('td')
                    cols = [ele.text.strip() for ele in cols]
                    if len(cols) == 5 and cols[2] != '':
                        if cols[3] != '':
                            data.append(float(cols[3].replace(',', '.')))
                        else:
                            data.append('NaN')
                self.nums.pop(self.num)
                self.db.insert_data(data)

        except urllib.request.HTTPError :
            print("HTTPERROR!")
        except urllib.request.URLError :
            print("URLERROR!")

def main():
    parser = Parser()

if __name__ == "__main__":
    sys.exit(main())
